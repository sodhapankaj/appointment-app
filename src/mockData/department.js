export const departmentData = [
  {
    Department_ID: 1,
    Department_name: "Homeopathy",
    Hospital_ID: 1,
    Doctor_name: "Dr. John B. Hanks",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Department_ID: 2,
    Department_name: "Dental",
    Hospital_ID: 2,
    Doctor_name: "Dr. Paul A. Levine",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Department_ID: 3,
    Department_name: "Physiotherapy",
    Hospital_ID: 1,
    Doctor_name: "Dr. Peyton T. Taylor",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Department_ID: 4,
    Department_name: "Dental",
    Hospital_ID: 5,
    Doctor_name: "Dr. Stacey E. Mills",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Department_ID: 5,
    Department_name: "Homeopathy",
    Hospital_ID: 4,
    Doctor_name: "Dr. Mark D. Okusa ",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Department_ID: 6,
    Department_name: "Skin care",
    Hospital_ID: 4,
    Doctor_name: "Dr. Suzanne Holroyd",
    Is_delete: 0,
    Created_at: new Date()
  }
];
