export const hospitalData = [
  {
    Hospital_id: 1,
    Hospital_name: "Newlife hospital",
    Hospital_address: "New York",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Hospital_id: 2,
    Hospital_name: "Care & Cure Hospital",
    Hospital_address: "Los Angeles - California ",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Hospital_id: 3,
    Hospital_name: "Medwin Cares",
    Hospital_address: "Chicago - Illinois",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Hospital_id: 4,
    Hospital_name: "Remedy plus care",
    Hospital_address: "Houston Texas",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Hospital_id: 5,
    Hospital_name: "Flowerence",
    Hospital_address: "Houston Texas",
    Is_delete: 0,
    Created_at: new Date()
  },
  {
    Hospital_id: 6,
    Hospital_name: "Rejuvenate",
    Hospital_address: "New York",
    Is_delete: 0,
    Created_at: new Date()
  }
];
