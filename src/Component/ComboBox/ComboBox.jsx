/* eslint-disable no-use-before-define */
import React from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";

export default function ComboBox({
  type = "",
  label,
  name = "",
  values = {},
  touched = {},
  errors = {},
  handleChange = () => {},
  options = [],
  setFieldValue = () => {},
  getOptionLabel = () => {}
}) {
  return (
    <Autocomplete
      id="combo-box-demo"
      options={options}
      value={values[name]}
      onChange={(event, newValue) => {
        setFieldValue(name, newValue);
      }}
      getOptionLabel={getOptionLabel}
      style={{ width: "100%" }}
      renderInput={params => (
        <TextField {...params} label={label} variant="outlined" name={name} />
      )}
    />
  );
}
