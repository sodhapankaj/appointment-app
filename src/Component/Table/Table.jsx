import MaterialTable from "material-table";

export default function Table({ columns, data, handleDelete, handleEdit }) {
  return (
    <MaterialTable
      columns={columns}
      data={data}
      style={{ width: "100%" }}
      actions={[
        {
          icon: "delete",
          tooltip: "delete",
          onClick: (event, rowData) => {
            handleDelete(rowData);
          }
        },
        {
          icon: "edit",
          tooltip: "edit",
          onClick: (event, rowData) => {
            handleEdit(rowData);
          }
        }
      ]}
      options={{
        sorting: true,
        actionsColumnIndex: 7
      }}
    />
  );
}
