import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import { formatDate } from "../../helper";

const useStyles = makeStyles(theme => ({
  textField: {
    width: "100%"
  }
}));

const TextInput = ({
  type = "",
  label,
  name = "",
  values = {},
  touched = {},
  errors = {},
  handleChange = () => {}
}) => {
  const classes = useStyles();
  const isError = touched[name] && errors[name] ? true : false;

  const date = new Date();
  const formattedDate = formatDate(date);

  const TodayDate = formattedDate;

  return (
    <TextField
      defaultValue={TodayDate}
      type={type}
      label={label}
      name={name}
      className={classes.textField}
      error={isError}
      helperText={isError && errors[name]}
      variant="outlined"
      value={values[name]}
      onChange={handleChange}
      InputLabelProps={{
        shrink: true
      }}
      inputProps={{
        min: TodayDate
      }}
    />
  );
};

export default TextInput;
