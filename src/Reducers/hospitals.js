import { hospitalData } from "../mockData/hospital";

export const ADD_HOSPITAL = "ADD_HOSPITAL";

const initialState = hospitalData;

const hospitals = (state = initialState, action) => {
  switch (action.type) {
    case ADD_HOSPITAL:
      return [...state, { ...action.payload }];

    default:
      return state;
  }
};

export default hospitals;
