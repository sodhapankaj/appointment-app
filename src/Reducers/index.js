import { combineReducers } from "redux";
import hospitals from "./hospitals";
import departments from "./departments";
import appointments from "./appointments";

const appReducers = combineReducers({
  hospitals,
  departments,
  appointments
});

export default appReducers;
