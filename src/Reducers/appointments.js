export const ADD_APPOINTMENTS = "ADD_APPOINTMENTS";
export const DELETE_APPOINTMENTS = "DELETE_APPOINTMENTS";

const initialState = [];

const appointments = (state = initialState, action) => {
  switch (action.type) {
    case ADD_APPOINTMENTS:
      return [
        ...state,
        {
          Appointment_ID: state.length + 1,
          Is_delete: 0,
          Created_at: new Date(),
          ...action.payload
        }
      ];
    case DELETE_APPOINTMENTS:
      return state.filter(appointment => {
        if (appointment.Appointment_ID != action.id) {
          return appointment;
        }
      });

    default:
      return state;
  }
};

export const add_appointments = payload => {
  return {
    type: ADD_APPOINTMENTS,
    payload
  };
};

export const delete_appointment = id => {
  return {
    type: DELETE_APPOINTMENTS,
    id
  };
};

export default appointments;
