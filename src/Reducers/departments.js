import { departmentData } from "../mockData/department";

export const ADD_DEPARTMENT = "ADD_DEPARTMENT";
export const UPDATE_PRODUCT = "UPDATE_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";

const initialState = departmentData;

const departments = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DEPARTMENT:
      return [...state, { ...action.payload }];

    default:
      return state;
  }
};

export default departments;
