import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./Containers/Login";
import AppointmentCreation from "./Containers/AppointmentCreation";
import Appointment from "./Containers/Appointments";

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={props => <Login {...props} />} />
        <Route path="/add-appointment" component={AppointmentCreation} />
        <Route path="/appointment" component={Appointment} />
      </Switch>
    </Router>
  );
};

export default Routes;
