import React, { Fragment } from "react";
import Grid from "@material-ui/core/Grid";
import { Typography, Box } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import TextInput from "../Component/TextInput/TextInput";
import { Formik, Form } from "formik";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginTop: 50
  }
}));

function Login({ match }) {
  const history = useHistory();

  const initailValues = {
    username: "user@gmail.com",
    password: "Abc@123@"
  };

  const classes = useStyles();
  const renderTitle = title => (
    <Typography style={{ marginBottom: 30, marginTop: 16 }} variant="h4">
      {title}
    </Typography>
  );

  const renderInput = props => <TextInput {...props} />;

  const handleSubmit = values => {
    history.push("/add-appointment");
  };

  return (
    <Fragment>
      <Grid container>
        <Grid item lg={12} md={12} style={{ padding: 30 }}>
          <Grid container justify="center">
            <Grid item lg={6} md={6}>
              <Paper elevation={3} className={classes.paper}>
                <Box textAlign="center">{renderTitle("Login")}</Box>
                <Formik initialValues={initailValues} onSubmit={handleSubmit}>
                  {({ handleChange, values, touched, errors }) => {
                    return (
                      <Form>
                        <Grid container spacing={2}>
                          <Grid item md={12} lg={12}>
                            {renderInput({
                              label: "Username",
                              name: "username",
                              values,
                              touched,
                              errors
                            })}
                          </Grid>
                          <Grid item md={12} lg={12}>
                            {renderInput({
                              label: "Password",
                              name: "password",
                              handleChange,
                              values,
                              type: "password",
                              touched,
                              errors
                            })}
                          </Grid>

                          <Grid item md={12} lg={12}>
                            <Button
                              type="submit"
                              fullWidth
                              variant="contained"
                              color="primary"
                            >
                              Login
                            </Button>
                          </Grid>
                        </Grid>
                      </Form>
                    );
                  }}
                </Formik>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default Login;
