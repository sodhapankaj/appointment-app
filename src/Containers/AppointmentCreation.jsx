import React, { Fragment, useEffect, useRef, useState } from "react";
import Grid from "@material-ui/core/Grid";
import { Typography, Box } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import TextInput from "../Component/TextInput/TextInput";
import * as yup from "yup";
import { Formik, Form } from "formik";
import Button from "@material-ui/core/Button";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ComboBox from "../Component/ComboBox/ComboBox";
import { add_appointments } from "../Reducers/appointments";
import MuiAlert from "@material-ui/lab/Alert";

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(2),
    marginTop: 50
  }
}));

function Appointment({ match }) {
  const formRef = useRef();

  const [isAlert, setAlert] = useState(false);

  const history = useHistory();

  const dispatch = useDispatch();

  const hospitals = useSelector(state => {
    return state.hospitals;
  });

  const appointments = useSelector(state => {
    return state.appointments;
  });

  const departments = useSelector(state => {
    return state.departments;
  });

  const initailValues = {
    hospitalName: "",
    departmentName: "",
    appointmentDate: "",
    appointmentTime: "",
    user: ""
  };

  const classes = useStyles();
  const renderTitle = title => (
    <Typography style={{ marginBottom: 30, marginTop: 16 }} variant="h4">
      {title}
    </Typography>
  );

  const renderInput = props => <TextInput {...props} />;

  const checkAppointmentAvalability = values => {
    const filteredAppointment = appointments.filter(appointment => {
      if (
        appointment.Appointment_date === values.appointmentDate &&
        appointment.appointmentTime === values.appointmentTime &&
        appointment.Department_ID === values.departmentName.Department_ID &&
        appointment.Hospital_ID === values.hospitalName.Hospital_id
      ) {
        return appointment;
      }
    });

    if (filteredAppointment.length !== 0) {
      return false;
    } else {
      return true;
    }
  };

  const handleSubmit = values => {
    const isAvailable = checkAppointmentAvalability(values);

    if (isAvailable) {
      setAlert(false);
      const params = {
        Department_ID: values.departmentName.Department_ID,
        Department_Name: values.departmentName.Department_name,
        Hospital_Name: values.hospitalName.Hospital_name,
        Hospital_ID: values.hospitalName.Hospital_id,
        User_email: "user@gmail.com",
        Appointment_date: values.appointmentDate,
        appointmentTime: values.appointmentTime
      };

      dispatch(add_appointments(params));
      history.push("/appointment");
    } else {
      setAlert(true);
    }
  };

  return (
    <Fragment>
      <Grid container>
        <Grid item lg={12} md={12} style={{ padding: 30 }}>
          <Grid container justify="center">
            <Grid item lg={6} md={6}>
              {isAlert && (
                <Alert severity="error">
                  your selected time is not available
                </Alert>
              )}

              <Paper elevation={3} className={classes.paper}>
                <Box textAlign="center">{renderTitle("Appointment")}</Box>
                <Formik
                  innerRef={formRef}
                  initialValues={initailValues}
                  onSubmit={handleSubmit}
                >
                  {({
                    handleChange,
                    values,
                    touched,
                    errors,
                    setFieldValue
                  }) => {
                    return (
                      <Form>
                        <Grid container spacing={2}>
                          <Grid item md={12} lg={12}>
                            <ComboBox
                              label={"Hospital Name"}
                              name="hospitalName"
                              handleChange={handleChange}
                              setFieldValue={setFieldValue}
                              options={hospitals}
                              values={values}
                              getOptionLabel={option => {
                                return `${option.Hospital_name}`;
                              }}
                            />
                          </Grid>
                          <Grid item md={12} lg={12}>
                            <ComboBox
                              label={"Department Name"}
                              name="departmentName"
                              handleChange={handleChange}
                              setFieldValue={setFieldValue}
                              options={departments}
                              values={values}
                              getOptionLabel={option => {
                                return `${option.Department_name}`;
                              }}
                            />
                          </Grid>
                          <Grid item md={12} lg={12}>
                            {renderInput({
                              label: "Appointment Date",
                              name: "appointmentDate",
                              handleChange,
                              type: "date",
                              values,
                              touched,
                              errors
                            })}
                          </Grid>

                          <Grid item md={12} lg={12}>
                            {renderInput({
                              label: "Appointment Time",
                              name: "appointmentTime",
                              handleChange,
                              type: "time",
                              values,
                              touched,
                              errors
                            })}
                          </Grid>

                          <Grid item md={12} lg={12}>
                            <Button
                              type="submit"
                              variant="contained"
                              color="primary"
                              fullWidth
                            >
                              Add Appointment
                            </Button>
                          </Grid>
                        </Grid>
                      </Form>
                    );
                  }}
                </Formik>
              </Paper>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default Appointment;
