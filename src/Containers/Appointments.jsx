import React, { Fragment, useEffect, useRef } from "react";
import Table from "../Component/Table/Table";
import { useDispatch, useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import { delete_appointment } from "../Reducers/appointments";

const columns = [
  { title: "Hospital Name", field: "Department_Name" },
  { title: "Department Name", field: "Hospital_Name" }
];

function Appointments() {
  const appointments = useSelector(state => {
    return state.appointments;
  });

  const dispatch = useDispatch();

  const handleDelete = rowData => {
    dispatch(delete_appointment(rowData.Appointment_ID));
  };

  return (
    <Fragment>
      <Grid container>
        <Grid item xl={12} lg={12} md={12} style={{ padding: 30 }}>
          <Grid container justify="center">
            <Table
              columns={columns}
              data={appointments}
              handleDelete={handleDelete}
            />
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default Appointments;
