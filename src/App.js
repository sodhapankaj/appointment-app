import React from "react";
import { Provider } from "react-redux";
import Routes from "./Routes";
import { store, persistor } from "./Store/Store";
import Header from "./Component/Header/Header";
import "./App.css";
import { PersistGate } from "redux-persist/integration/react";

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Header />
        <Routes />
      </PersistGate>
    </Provider>
  );
}

export default App;
